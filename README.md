# The Dad Jokes API

To get started clone the Git Repository onto your machine. Then to get started you need to install poetry using pip.
```sh
pip install poetry
```

Then we want to create a virtual environment using poetry
```sh
poetry shell
```
Finally, to get the project dependencies install run
```sh
poetry install 
```

To run the application use the following command.
```sh
uvicorn app:app --reload
```


Build the docker image 
```sh 
docker build . -t app
```
Then, run the container
```sh 
docker run -p 80:80 app
```

